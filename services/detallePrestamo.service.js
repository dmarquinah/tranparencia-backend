'use strict';

const setupBaseService = require('./base.service');
const { Op } = require("sequelize");

module.exports = function setupDetallePrestamoService(detallePrestamoModel, empresaModel, entidadCreditoModel, departamentoModel, sectorModel, tipoEntidadCreditoModel) {
  let baseService = new setupBaseService();

  //#region Helpers
  function getSimpleListaPrestamoModel(model) {
    return {
      idDetallePrestamo: model.idDetallePrestamo,
      NombreEmpresa: model.NombreEmpresa,
      RUC: model.RUC,
      prestamoOtorgado: model.montoPrestamo,
      coberturaPrestamo: model.montoCobertura,
      nombreDepartamento: model.nombreDepartamento
    };
  }

  function getSimpleDetallePrestamoModel(model) {
    return {
      idDetallePrestamo: model.idDetallePrestamo,
      NombreEmpresa: model.NombreEmpresa,
      RUC: model.RUC,
      nombreOtorgante: model.nombreEntidadCredito,
      tipoEntidadOtorgante: model.tipoEntidadCredito,
      prestamoOtorgado: model.montoPrestamo,
      coberturaPrestamo: model.montoCobertura,
      nombreDepartamento: model.nombreDepartamento
    };
  }

  function getOrderArray(orderby, ordertype, detallePrestamoModel, empresaModel) {
    let arr = [];
    switch (orderby) {
      case 'NombreEmpresa':
        arr = [detallePrestamoModel.associations.Empresa, orderby, ordertype]
        break;
      case 'RUC':
        arr = [orderby, ordertype]
        break;
      case 'prestamoOtorgado':
        arr = [orderby, ordertype]
        break;
      case 'coberturaPrestamo':
        arr = [orderby, ordertype]
        break;
      case 'nombreDepartamento':
        arr = [detallePrestamoModel.associations.Empresa, empresaModel.associations.Departamento, orderby, ordertype]
        break;
      default:
        break;
    }

    return arr;
  }
  //#endregion

  async function doListarPrestamosService(limit, page, orderby, ordertype) {
    try {
      const listaPrestamos = await detallePrestamoModel.findAll({
        order: getOrderArray(orderby, ordertype, detallePrestamoModel, empresaModel),
        limit: parseInt(limit),
        offset: parseInt(limit)*(parseInt(page)-1),
        include: [
          {
            model: empresaModel,
            include: [
              {
                model: departamentoModel
              }
            ]
          }
        ]
      });

      return baseService.getServiceResponse(200, "Success", listaPrestamos.map(c => getSimpleListaPrestamoModel(c)));

    } catch (err) {
      console.log('Error: ', err);
      return baseService.getServiceResponse(500, err, {});
    }
  }

  async function doDetallePrestamoService(idEmpresa) {
    try {
      const detallePrestamo = await detallePrestamoModel.findAll({
        where: {
          RUC: {
            [Op.eq]: idEmpresa
          }
        },
        include: [
          {
            model: empresaModel,
            include: [
              {
                model: departamentoModel,
                required: true
              },
              {
                model: sectorModel,
                required: true
              }
            ]
          },
          {
            model: entidadCreditoModel,
            include: [
              {
                model: tipoEntidadCreditoModel,
                required: true
              }
            ]
          }
        ]
      });

      return baseService.getServiceResponse(200, "Success", detallePrestamo.map(c => getSimpleDetallePrestamoModel(c)));

    } catch (err) {
      console.log('Error: ', err);
      return baseService.getServiceResponse(500, err, {});
    }
  }

  return {
    doListarPrestamosService,
    doDetallePrestamoService
  };
}
