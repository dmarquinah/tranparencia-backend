'use strict';

const setupServices = require('./');

let services;

module.exports = async function serviceContainer(serviceName) {
  if (!services) {
    services = await setupServices();
  }
  // Return requested service
  switch (serviceName) {
    case 'empresa':
      return services.empresaService;
    case 'articulo':
      return services.articuloService;
    case 'departamento':
      return services.departamentoService;
    case 'detalle_prestamo':
      return services.detallePrestamoService;
    case 'entidad_credito':
      return services.entidadCreditoService;
    case 'metrica_precedente':
      return services.metricaPrecedenteService;
    case 'precedentes':
      return services.precedenteService;
    case 'sector':
      return services.sectorService;
    case 'tipo_entidad_credito':
      return services.tipoEntidadCreditoService;
    case 'usuario':
      return services.usuarioService;
    case 'voto_usuario':
      return services.votoUsuarioService;
  }
}
