'use strict';

const setupBaseService = require('./base.service');

module.exports = function setupPrecedenteService(precedenteModel, detallePrestamoModel, empresaModel, metricaPrecedenteModel, usuarioModel, articuloModel) {
  let baseService = new setupBaseService();

  //#region Helpers
  function getSimplePrecedenteModel(model) {
    return {
      idPrecedente: model.idPrecedente,
      titulo: model.titulo,
      descripcion: model.descripcion,
      fechaEnvio: model.fechaEnvio,
      votoPositivo: model.votoPositivo,
      idUsuario: model.mostrarUsuario ? model.idUsuario : null,
      nombreUsuario: model.mostrarUsuario ? model.nombre: null,
      apellidosUsuario: model.mostrarUsuario ? model.apellidos: null,
      fechaCreacion: model.fechaCreacion,
      activo: model.estado
    };
  }
  //#endregion

  async function doListaPrecedenteService(ruc) {
    try {
      const precedente = await precedenteModel.findAll({
        where: {
          RUC: ruc
        },
        include: [{
          model: detallePrestamoModel,
          required: true
        },{
          model: metricaPrecedenteModel,
          required: true
        },{
          model: usuarioModel,
          required: false
        }]
      });

      return baseService.getServiceResponse(200, "Success", precedente.map(c => getSimpleDetalleEmpresaModel(c)));
    } catch (err) {
      console.log('Error: ', err);
      return baseService.getServiceResponse(500, err, {});
    }
  }

  async function doCreatePrecedenteEmpresa(RUC, body) {
    //parámetro body tendrá la estructura:
      /*
        body {
          titulo: '',
          descripcion: '',
          articulos: [
            'link1.com/articulo',
            'link2.com/articulo'
          ]
        }
      */
    //let transaction;
    try {
      //transaction = await Sequelize.transaction();
      
      //Se crea una metrica asociada al precedente, eso devolverá un idMetrica
      let metrica = await metricaPrecedenteModel.create(
        {
          votoPositivo: 0
        } //, {transaction}
      );
      //metrica.save();

      //Las fechas son creadas en el instante que se procesa en el backend:
      let fechaEnvio = new Date().getTime();
      
      //Se crea un precedente, se debe usar idMetrica devuelto (metrica.idMetrica)
      let precedente = await precedenteModel.create(
        {
          RUC: RUC,
          titulo: body.titulo,
          descripcion: body.descripcion,
          estado: false,
          fechaEnvio,
          fechaRevision: null,
          idMetrica : metrica.idMetrica,
          //aquí queda relacionar idusuario, por ahora null porque no se ha definido
          mostrarUsuario: false
        } //, {transaction}
      );
      //precedente.save();

      //Con el nuevo idPrecedente (precedente.idPrecedente) se crean los articulos asociados
      /*
      let articulos = await articuloModel.create(
        {
          idPrecedente: precedente.idPrecedente,
          url: body.articulos[0],
          fechaCreacion: fechaEnvio
        } //, {transaction}
      );
      //articulos.save();
      */
      
      for (const articulo of body.articulos) {
        let articulos = await articuloModel.findOrCreate(
          {
            where: { url: articulo },
            defaults: {
              idPrecedente: precedente.idPrecedente,
              fechaCreacion: fechaEnvio
            }
          }
        );
      }
      
      let nuevoPrecedente = await precedenteModel.findOne({
        include: { all: true },
        where: { RUC }
      });
      //Si todo fue bien, se obtiene el nuevo precedente

      //await transaction.commit();

      return baseService.getServiceResponse(200, "Se creó exitosamente el registro", {
        idPrecedente: nuevoPrecedente.idPrecedente
      });
    } catch (err) {
      console.log('Error: ', err);
      //if (transaction) await transaction.rollback();
      return baseService.getServiceResponse(500, err, {});
    }
    
  }

  return {
    doListaPrecedenteService,
    doCreatePrecedenteEmpresa
  };
}