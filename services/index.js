'use strict';

const dbInstance = require('./../models');

const setupPrecedentesService = require('./precedente.service');
const setupEmpresaService = require('./empresa.service');
const setupDetallePrestamoService = require('./detallePrestamo.service');

module.exports = async function () {
  const precedenteService = setupPrecedentesService(dbInstance.Precedente, dbInstance.Detalle_Prestamo, dbInstance.Empresa, dbInstance.Metrica_Precedente, dbInstance.Usuario, dbInstance.Articulo);
  const empresaService = setupEmpresaService(dbInstance.Empresa, dbInstance.Departamento, dbInstance.Sector);
  const detallePrestamoService = setupDetallePrestamoService(dbInstance.Detalle_Prestamo, dbInstance.Empresa, dbInstance.Entidad_Credito, dbInstance.Departamento, dbInstance.Sector, dbInstance.Tipo_Entidad_Credito);

  return {
    precedenteService,
    empresaService,
    detallePrestamoService
  };
};
