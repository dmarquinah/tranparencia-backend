'use strict';

const setupBaseService = require('./base.service');
const sequelize = require('sequelize');

module.exports = function setupEmpresaService(empresaModel, departamentoModel, sectorModel) {
  let baseService = new setupBaseService();

  //#region Helpers
  function getSimpleDetalleEmpresaModel(model) {
    return {
      NombreEmpresa: model.NombreEmpresa,
      RUC: model.RUC,
      nombreDepartamento: model.Departamento.nombreDepartamento,
      sector: model.Sector.sector
    };
  }
  //#endregion

  async function doDetalleEmpresaService(idEmpresa) {
    try {
      const detalleEmpresa = await empresaModel.findAll({
        where: {
          RUC: idEmpresa
        },
        include: [{
          model: departamentoModel,
          as: 'Departamento',
          attributes: ['nombreDepartamento'],
          required: true
        },{
          model: sectorModel,
          as: 'Sector',
          attributes: ['sector'],
          required: true
        }]
      });
      return baseService.getServiceResponse(200, "Success", detalleEmpresa.map(c => getSimpleDetalleEmpresaModel(c)));
    } catch (err) {
      console.log('Error: ', err);
      return baseService.getServiceResponse(500, err, {});
    }
  }

  return {
    doDetalleEmpresaService
  };
}