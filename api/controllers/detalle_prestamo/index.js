'use strict';

const express = require('express');
const detallePrestamoController = require('./detallePrestamo.controller');

const router = express.Router();

router.get('/', detallePrestamoController.getListaPrestamos);
router.get('/:id', detallePrestamoController.getDetallePrestamoEmpresa);

module.exports = router;
