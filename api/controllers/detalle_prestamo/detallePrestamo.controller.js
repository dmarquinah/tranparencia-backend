'use strict';

const setupBaseController = require('../base.controller');
const serviceContainer = require('./../../../services/service.container');

let baseController = new setupBaseController();

const getListaPrestamos = async (request, response) => {
  let responseCode, responseData;

  if (!request.query.limit || isNaN(request.query.limit) || !request.query.page || isNaN(request.query.page) || request.query.limit > 100) {
    return response
      .status(400)
      .json(baseController.getErrorResponse('Parámetros de paginación incorrectos'));
  }

  try {
    const detallePrestamoService = await serviceContainer('detalle_prestamo');
    const infodetallePrestamo = await detallePrestamoService.doListarPrestamosService(request.query.limit, request.query.page, request.query.orderby, request.query.ordertype);

    responseCode = infodetallePrestamo.responseCode;
    responseData = baseController.getSuccessResponse(
      infodetallePrestamo.data,
      infodetallePrestamo.message
    );
  } catch (err) {
    console.error('Error al obtener info de empresa: ', err);
    responseData = baseController.getErrorResponse('Error al obtener info de empresa');
  }

  return response
    .status(responseCode)
    .json(responseData);
}

const getDetallePrestamoEmpresa = async (request, response) => {

  if (!request.params.id) {
    return response
      .status(400)
      .json(baseController.getErrorResponse('Parámetro id no válido'));
  }

  try {
    const detallePrestamoService = await serviceContainer('detalle_prestamo');
    const infodetallePrestamo = await detallePrestamoService.doDetallePrestamoService(request.params.id);

    responseCode = infodetallePrestamo.responseCode;
    responseData = baseController.getSuccessResponse(
      infodetallePrestamo.data,
      infoEmpresa.message
    );
  } catch (err) {
    console.error('Error al obtener info de empresa: ', err);
    responseData = baseController.getErrorResponse('Error al obtener info de empresa');
  }

  return response
    .status(responseCode)
    .json(responseData);
};

module.exports = {
  getListaPrestamos,
  getDetallePrestamoEmpresa
  // post,
  // update,
  // remove,
  // changePassword
};
