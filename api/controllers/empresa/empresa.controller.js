'use strict';

const setupBaseController = require('../base.controller');
const serviceContainer = require('./../../../services/service.container');

let baseController = new setupBaseController();

const getDetalleEmpresa = async (request, response) => {

  if (!request.params.id) {
    return response
      .status(400)
      .json(baseController.getErrorResponse('Parámetro id no válido'));
  }
  let responseCode, responseData;
  try {
    const empresaService = await serviceContainer('empresa');
    const infoEmpresa = await empresaService.doDetalleEmpresaService(request.params.id);
    responseCode = infoEmpresa.responseCode;
    responseData = baseController.getSuccessResponse(
      infoEmpresa.data,
      infoEmpresa.message
    );
  } catch (err) {
    console.error('Error al obtener info de empresa: ', err);
    responseData = baseController.getErrorResponse('Error al obtener info de empresa');
  }

  return response
    .status(responseCode)
    .json(responseData);
};

module.exports = {
  getDetalleEmpresa,
  // post,
  // update,
  // remove,
  // changePassword
};
