'use strict';

const express = require('express');
const empresaController = require('./empresa.controller');

const router = express.Router();

router.get('/:id', empresaController.getDetalleEmpresa);

module.exports = router;
