'use strict';

const express = require('express');
const router = express.Router();

router.use('/empresa', require('./empresa'));
router.use('/precedente', require('./precedentes'));
router.use('/detallePrestamo', require('./detalle_prestamo'));

module.exports = router;
