'use strict';

const setupBaseController = require('./../base.controller');
const serviceContainer = require('./../../../services/service.container');

let baseController = new setupBaseController();

const getPrecedentesEmpresa = async (request, response) => {
  let responseCode;
  let responseData;
  try {
    // Inject services
    const precedentesService = await serviceContainer('precedentes');
    // Get Precedentes
    const precedentesData = await precedentesService.doListPrecedentesEmpresa(request.params.idEmpresa);
    // Return the data
    responseCode = precedentesData.responseCode;
    responseData = baseController.getSuccessResponse(precedentesData.data, precedentesData.message);
  } catch (err) {
    console.error('Error: ', err);
    responseCode = 500;
    responseData = baseController.getErrorResponse('Error');
  }
  return response.status(responseCode).json(responseData);
};

const postPrecedenteEmpresa = async (request, response) => {
  let responseCode;
  let responseData;
  try {
    // Inject services
    const precedentesService = await serviceContainer('precedentes');
    // Get Precedentes
    const precedentesData = await precedentesService.doCreatePrecedenteEmpresa(request.params.idEmpresa, request.body);
    // Return the data
    responseCode = precedentesData.responseCode;
    responseData = baseController.getSuccessResponse(precedentesData.data, precedentesData.message);
  } catch (err) {
    console.error('Error: ', err);
    responseCode = 500;
    responseData = baseController.getErrorResponse('Error');
  }
  return response.status(responseCode).json(responseData);
};

module.exports = {
  getPrecedentesEmpresa,
  postPrecedenteEmpresa
};
