'use strict';

const express = require('express');
const precedentesController = require('./precedentes.controller');

const router = express.Router();

router.get('/:idEmpresa', precedentesController.getPrecedentesEmpresa);
router.post('/:idEmpresa/crear', precedentesController.postPrecedenteEmpresa);
//router.post('/:id', precedentesController.crearPrecedenteEmpresa); //Con usuario

module.exports = router;