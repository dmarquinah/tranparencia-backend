module.exports = {
  dialect: process.env.DIALECT,
  DATABASE: process.env.DATABASE,
  DB_USERNAME: process.env.DB_USERNAME,
  DB_PASSWORD: process.env.DB_PASSWORD,
  HOST: process.env.HOST,
  DB_PORT: process.env.DB_PORT,
  "operatorsAliases": '0',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};