/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  let Tipo_Entidad_Credito = sequelize.define('Tipo_Entidad_Credito', {
    idTipoEntidad: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    tipoEntidadCredito: {
      type: DataTypes.STRING(40),
      allowNull: false
    }
  }, {
    tableName: 'Tipo_Entidad_Credito'
  });

  Tipo_Entidad_Credito.associate = function (models) {
    Tipo_Entidad_Credito.belongsTo(models.Entidad_Credito, {
      foreignKey: 'idTipoEntidad',
      targetKey: 'idTipoEntidad'
    });
  }

  return Tipo_Entidad_Credito;
};
