/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
   let Sector = sequelize.define('Sector', {
    idSector: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Empresa',
        key: 'idSector'
      }
    },
    sector: {
      type: DataTypes.STRING(75),
      allowNull: false
    }
  }, {
    tableName: 'Sector'
  });

  Sector.association = function (models) {
    Sector.belongsTo(models.Empresa, {
      foreignKey: 'idSector',
      targetKey: 'idSector'
    });
  }

  return Sector;
};
