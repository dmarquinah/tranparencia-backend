/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  let Metrica_Precedente = sequelize.define('Metrica_Precedente', {
    idMetrica: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true //incrementé esto
    },
    votoPositivo: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }
  }, {
    tableName: 'Metrica_Precedente'
  });

  Metrica_Precedente.associate = function (models) {
    Metrica_Precedente.belongsTo(models.Precedente, {
      foreignKey: 'idMetrica',
      targetKey: 'idMetrica'
    });
    Metrica_Precedente.hasMany(models.Voto_Usuario, {
      sourceKey: 'idMetrica',
      foreignKey: 'idMetrica'
    });
  }

  return Metrica_Precedente;
};
