/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  let Voto_Usuario = sequelize.define('Voto_Usuario', {
    idUsuario: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Usuario',
        key: 'idUsuario'
      }
    },
    idMetrica: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Metrica_Precedente',
        key: 'idMetrica'
      }
    },
    fechaVoto: {
      type: DataTypes.BIGINT,
      allowNull: false
    }
  }, {
    tableName: 'Voto_Usuario'
  });

  Voto_Usuario.associate = function (models) {
    Voto_Usuario.belongsTo(models.Usuario, {
      foreignKey: 'idUsuario',
      targetKey: 'idUsuario'
    });
    Voto_Usuario.belongsTo(models.Metrica_Precedente, {
      foreignKey: 'idMetrica',
      targetKey: 'idMetrica'
    });
  }

  return Voto_Usuario;
};
