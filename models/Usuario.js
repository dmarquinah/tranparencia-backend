/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  let Usuario = sequelize.define('Usuario', {
    idUsuario: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    correo: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    nombres: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    apellidos: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    fechaCreacion: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    activo: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    tableName: 'Usuario'
  });

  Usuario.associate = function (models) {
    Usuario.hasMany(models.Voto_Usuario, {
      sourceKey: 'idUsuario',
      foreignKey: 'idUsuario'
    });
    Usuario.hasMany(models.Precedente, {
      sourceKey: 'idUsuario',
      foreignKey: 'idUsuario'
    });
  }

  return Usuario;
};
