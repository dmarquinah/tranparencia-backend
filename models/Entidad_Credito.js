/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  let Entidad_Credito = sequelize.define('Entidad_Credito', {
    idEntidadCredito: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    nombreEntidadCredito: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    idTipoEntidad: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Tipo_Entidad_Credito',
        key: 'idTipoEntidad'
      }
    }
  }, {
    tableName: 'Entidad_Credito'
  });

  Entidad_Credito.associate = function (models) {
    Entidad_Credito.hasOne(models.Tipo_Entidad_Credito, {
      sourceKey: 'idTipoEntidad',
      foreignKey: 'idTipoEntidad'
    });
    Entidad_Credito.belongsTo(models.Detalle_Prestamo, {
      foreignKey: 'idEntidadCredito',
      targetKey: 'idEntidadCredito'
    });
  }

  return Entidad_Credito;
};
