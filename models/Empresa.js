/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  let Empresa = sequelize.define('Empresa', {
    RUC: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    NombreEmpresa: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    idDepartamento: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Empresa',
        key: 'idDepartamento'
      }
    },
    idSector: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }
  }, {
    tableName: 'Empresa'
  });

  Empresa.associate = function (models) {
    Empresa.hasOne(models.Departamento, {
      sourceKey: 'idDepartamento',
      foreignKey: 'idDepartamento'
    });
    Empresa.hasOne(models.Sector, {
      sourceKey: 'idSector',
      foreignKey: 'idSector'
    });
    Empresa.hasOne(models.Detalle_Prestamo, {
      sourceKey: 'RUC',
      foreignKey: 'RUC'
    });
    Empresa.hasMany(models.Precedente, {
      sourceKey: 'RUC',
      foreignKey: 'RUC'
    });
  }


  return Empresa;
};
