'use strict';

/*
 * Loads all models and make them available in the sequelize object
 * @module models
 */

const fs = require('fs');
const path = require('path');
const dbConfig = require('../config/db.config');
const Sequelize = require('sequelize');
const sequelize = new Sequelize( dbConfig.DATABASE, dbConfig.DB_USERNAME, dbConfig.DB_PASSWORD, {
    host: dbConfig.HOST,
    dialect: 'mysql',
    omitNull: true,
    operatorsAliases: dbConfig.operatorsAliases,
    define: {
      timestamps: false
    },
    pool: {
      max: dbConfig.pool.max,
      min: dbConfig.pool.min,
      acquire: dbConfig.pool.acquire,
      idle: dbConfig.pool.idle
    }
  }
);
const db = {};

/*
 * Find all models and put them in an array:
 * - Filter all files inside the current folder which arent dot files, aren't index.js(this file) and aren't tests.
 * - Remaining files are the models to be loaded
 */
fs
  .readdirSync(__dirname)
  .filter(function(file) { 
    return (file.indexOf(".") !== 0) && (file !== "index.js") && (file.indexOf('test') === -1) && (file.indexOf('database.js') === -1);
  })
  .forEach(function(file) {
    let model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

// Configure associations between models
Object.keys(db).forEach(function(modelName) {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;