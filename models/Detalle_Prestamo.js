/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  let Detalle_Prestamo = sequelize.define('Detalle_Prestamo', {
    idDetallePrestamo: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    RUC: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'Empresa',
        key: 'RUC'
      }
    },
    idEntidadCredito: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Entidad_Credito',
        key: 'idEntidadCredito'
      }
    },
    montoPrestamo: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    montoCobertura: {
      type: DataTypes.DECIMAL,
      allowNull: true
    }
  }, {
    tableName: 'Detalle_Prestamo'
  });

  Detalle_Prestamo.associate = function (models) {
    Detalle_Prestamo.belongsTo(models.Empresa, {
      foreignKey: 'RUC',
      targetKey: 'RUC'
    });
    Detalle_Prestamo.hasOne(models.Entidad_Credito, {
      sourceKey: 'idEntidadCredito',
      foreignKey: 'idEntidadCredito'
    });
  }

  return Detalle_Prestamo;
};
