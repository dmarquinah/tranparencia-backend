/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  let Articulo = sequelize.define('Articulo', {
    idArticulo: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true //incrementé esto
    },
    idPrecedente: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'Precedente',
        key: 'idPrecedente'
      }
    },
    url: {
      type: DataTypes.STRING(255),
      allowNull: false,
      unique: true
    },
    fechaCreacion: {
      type: DataTypes.STRING(45),
      allowNull: true
    }
  }, {
    tableName: 'Articulo'
  });

  Articulo.associate = function (models) {
    Articulo.belongsTo(models.Precedente, {
      foreignKey: 'idPrecedente',
      targetKey: 'idPrecedente'
    });
  }

  return Articulo;
};
