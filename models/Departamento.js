/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  let Departamento = sequelize.define('Departamento', {
    idDepartamento: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Empresa',
        key: 'idDepartamento'
      }
    },
    nombreDepartamento: {
      type: DataTypes.STRING(100),
      allowNull: false
    }
  }, {
    tableName: 'Departamento'
  });

  Departamento.associate = function (models) {
    Departamento.belongsTo(models.Empresa, {
      foreignKey: 'idDepartamento',
      targetKey: 'idDepartamento'
    });
  }

  return Departamento;
};
