/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  let Precedente = sequelize.define('Precedente', {
    idPrecedente: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true //incrementé esto
    },
    RUC: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'Empresa',
        key: 'RUC'
      }
    },
    titulo: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    descripcion: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    estado: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    fechaEnvio: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    fechaRevision: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    idMetrica: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      //autoIncrement: true, //incrementé esto
      references: {
        model: 'Metrica_Precedente',
        key: 'idMetrica'
      }
    },
    idUsuario: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Usuario',
        key: 'idUsuario'
      }
    },
    mostrarUsuario: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    tableName: 'Precedente'
  });

  Precedente.associate = function (models) {
    Precedente.belongsTo(models.Empresa, {
      foreignKey: 'RUC',
      targetKey: 'RUC'
    });
    Precedente.hasMany(models.Articulo, {
      sourceKey: 'idPrecedente',
      foreignKey: 'idPrecedente'
    });
    Precedente.hasOne(models.Metrica_Precedente, {
      sourceKey: 'idMetrica',
      foreignKey: 'idMetrica'
    });
    Precedente.belongsTo(models.Usuario, {
      foreignKey: 'idUsuario',
      targetKey: 'idUsuario'
    });
  }

  return Precedente;
};
